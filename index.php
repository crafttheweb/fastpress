<?php

namespace FastPress;

/*
 * Plugin Name: FastPress
 * Description: A fast backend for common WordPress queries
 * Version: 0.1
 * Author: The Fold
 * Author URI: thefold.co.nz
 */

require_once ABSPATH.'/../../vendor/autoload.php';

use \TheFold\FastPress;

function getInstance()
{
    return FastPress::get_instance();
}

function index_post(\WP_Post $post)
{
    return FastPress::get_instance()->index_post($post);
}

function delete_post($post_id)
{
    if($post_id instanceof \WP_Post){
        $post_id = $post_id->ID;
    }

    return FastPress::get_instance()->delete_post($post_id);
}

function get_posts($args=[])
{
    return FastPress::get_instance()->get_posts($args);
}

function set_facets($args)
{
    return FastPress::get_instance()->set_facets($args);
}

function get_facets()
{
    return FastPress::get_instance()->get_facets();
}

function get_stats()
{
    return FastPress::get_instance()->get_stats();
}

function get_facet($name, $qparams=null,$reuse=true)
{
    return FastPress::get_instance()->get_facet($name,$qparams,$reuse);
}

function delete_all($query=null)
{
    return FastPress::get_instance()->delete_all($query);
}

function search($query,$args=[])
{
    return FastPress::get_instance()->search($query,$args);
}

function get_paging(\Closure $format_function = null)
{
    return FastPress::get_instance()->get_paging_links($format_function);
}

function query_posts($args)
{
    return FastPress::get_instance()->query_posts($args);
}

function the_post()
{
    return FastPress::get_instance()->the_post();
}

function have_posts()
{
    return FastPress::get_instance()->have_posts();
}

function next_post()
{
    return FastPress::get_instance()->next_post();
}

//bad name
function get_previous_posts_link( $label = null ) {
    return get_previous_page_link($label);
}

function get_previous_page_link( $label = null ) {
    return FastPress::get_instance()->get_previous_posts_link($label);
}

//bad name
function get_next_posts_link( $label = null ) {
   return get_next_page_link($label);
}

function get_next_page_link( $label = null ) {

    return FastPress::get_instance()->get_next_posts_link($label);
}

function index_user(\WP_User $user)
{
    return FastPress::get_instance()->index_user($user);
}

function delete_user($user_id)
{
    return FastPress::get_instance()->delete_user($user_id);
}

function get_users($params=[])
{
    return FastPress::get_instance()->get_users($params);
}

if(is_admin()){
    FastPress::get_instance()->admin_init();
}

//TODO might need some more hooks here


add_action('profile_update',function($user_id){

   index_user(get_userdata($user_id)); 
});

add_action('edit_user_profile_update',function($user_id){

   index_user(get_userdata($user_id)); 
});

//todo is it possible for role to be passed empty ?! I'm guessing it is!
add_action('set_user_role',function($user_id,$role,$old_roles){

    $indexable_roles = (array) \TheFold\WordPress::get_option(FastPress::SETTING_NAMESPACE,'user_roles');

    $roles = [];

    if(is_multisite()){

        $sites = wp_get_sites();

        if($siterole = get_user_meta($user_id, 'wp_capabilities',true)){
            $roles = array_keys($siterole);
        }
        
        foreach($sites as $site){
            if($siterole = get_user_meta($user_id, 'wp_'.$site['blog_id'].'_capabilities', true)){
                $roles = array_merge($roles, array_keys($siterole));
            }
        }
    }

    $roles[] = $role;

    //Role is indexable
    if(array_intersect($roles, $indexable_roles)) {
        index_user(get_userdata($user_id));
    } else {

        delete_user($user_id);
    }

},10,3);

add_action('delete_user',function($user_id){
    delete_user($user_id);
});

add_action('save_post', function($post_id){

    if($post = get_post($post_id)){

        index_post($post);
    }
});

add_action('updated_postmeta', function($meta_id, $post_id){

    if($post = get_post($post_id)){

        index_post($post);
    }
},10,2);

add_action('wp_trash_post',function($post_id){

    delete_post($post_id);
});

/*
 * TODO set this up
\TheFold\WordPress::init_url_access([

    apply_filters('fastpress_url_search','search') => function(){
        
        search();
    }
]);
 
 */
